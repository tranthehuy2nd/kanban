import { compose } from 'redux';
import injectSaga from '../../utils/injectSaga';
import saga from './saga';
import HomePage from './HomePage';

// export default HomePage;
const withSaga = injectSaga({ key: 'home', saga });

export default compose(withSaga)(HomePage);
