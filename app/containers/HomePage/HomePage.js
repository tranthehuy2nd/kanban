import React, { Component } from 'react';
import * as qs from 'query-string';
import { push } from 'react-router-redux';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { Button, Glyphicon, FormGroup, FormControl } from 'react-bootstrap';
import Dialog from 'react-bootstrap-dialog';
import HorizontalList from '../../components/HorizontalList';
import { uuid } from '../../utils';
import './style.scss';

const mapDispatchToProps = (dispatch) => ({
  dispatch,
});

@connect((state) => ({
  boards: state
    .get('boards')
    .toJS(),
  columns: state
    .get('columns')
    .toJS(),
  tasks: state
    .get('tasks')
    .toJS(),
}), mapDispatchToProps)
export default class HomePage extends Component {
  state = {
    newColumnName: '',
  }

  componentDidMount() {
    // fix on static page like github
    const params = qs.parse(this.props.location.search);
    if (params.p) {
      this.props.dispatch(push(params.p));
    }
    console.log(this.props);
  }

  componentDidUpdate() {
    if (this.refreshHorizontalList) {
      this.refreshHorizontalList();
    }
  }

  onDragEnd = (result) => {
    if (result.type === 'COLUMN') {
      this.props.dispatch({
        type: 'SWAP_COLUMNS',
        boardId: 'b0',
        sourceColumnIndex: result.source.index,
        destinationColumnIndex: result.destination.index,
      });
    }
    if (result.type === 'TASK') {
      this.props.dispatch({
        type: 'MOVE_TASKS',
        sourceColumnId: result.source.droppableId,
        sourceTaskIndex: result.source.index,
        destinationColumnId: result.destination.droppableId,
        destinationTaskIndex: result.destination.index,
      });
    }
  }

  onAddNewTask = (columnId) => {
    this.props.dispatch({
      type: 'ADD_NEW_TASK',
      columnId,
      taskId: uuid(),
      name: this.state[`newTask${columnId}`],
    });
    this.setState({ [`newTask${columnId}`]: '' });
  }

  onAddNewColumn = () => {
    this.props.dispatch({
      type: 'ADD_NEW_COLUMN',
      boardId: 'b0',
      columnId: uuid(),
      name: this.state.newColumnName,
    });
    this.setState({ newColumnName: '' });
  }

  datasource = (type) => {
    if (!this.props[type]) return {};
    return {
      ...this.props[type],
      get: (id) => this.props[type].list[id],
    };
  };

  renderActionColumn = () => {
    return (
      <div className="home-page__add-task-row">
        <form onSubmit={(e) => { e.preventDefault(); this.onAddNewColumn(); }} >
          <FormGroup bsSize="small">
            <FormControl
              value={this.state.newColumnName}
              onChange={(e) => this.setState({ newColumnName: e.target.value })}
              type="text"
              placeholder="Type column name and press enter"
            />
          </FormGroup>
        </form>
      </div>
    );
  }

  // renderActionColumn = () => {
  //   return (
  //     <div>
  //       <Button
  //         onClick={() => this.dialog.show({
  //           prompt: Dialog.TextPrompt({
  //             placeholder: 'Enter new board name'
  //           }),
  //           actions: [
  //             Dialog.CancelAction(),
  //             Dialog.OKAction((dialog) => {
  //               const result = dialog.value;
  //               console.log(`okay! result is "${result}".`);
  //             })
  //           ],
  //         })}
  //         bsStyle="success"
  //         block
  //       >
  //         <Glyphicon glyph="plus" /> Add new column
  //       </Button>
  //     </div>
  //   );
  // }

  renderActionRow = (id) => {
    return (
      <div className="home-page__add-task-row">
        <div className="home-page__action-buttons">

          <Button bsStyle="success">
            <Glyphicon glyph="asterisk" />
          </Button>

          <Button >
            <Glyphicon glyph="pencil" />
          </Button>

          <Button >
            <Glyphicon glyph="duplicate" />
          </Button>

          <Button >
            <Glyphicon glyph="scissors" />
          </Button>

          <Button bsStyle="danger" >
            <Glyphicon glyph="trash" />
          </Button>
        </div>
        <form onSubmit={(e) => { e.preventDefault(); this.onAddNewTask(id); }} >
          <FormGroup>
            <FormControl
              value={!this.state[`newTask${id}`] ? '' : this.state[`newTask${id}`]}
              onChange={(e) => this.setState({ [`newTask${id}`]: e.target.value })}
              type="text"
              placeholder="Type task name and press enter"
              bsSize="small"
            />
          </FormGroup>
        </form>
      </div>
    );
  }

  render() {
    const board = this.datasource('boards').get('b0');
    return (
      <article>
        <Helmet>
          <title>Home Page</title>
          <meta name="description" content="Homepage" />
        </Helmet>
        <div className="home-page" style={{ backgroundImage: 'url(https://picsum.photos/1024/768?random)' }}>
          <HorizontalList
            board={board}
            datasource={this.datasource}
            columns={this.props.columns.list}
            tasks={this.props.tasks.list}
            actionColumn={this.renderActionColumn()}
            actionRow={this.renderActionRow}
            setRefresh={(f) => { this.refreshHorizontalList = f; }}
            onDragEnd={this.onDragEnd}
          />
          <Dialog ref={(el) => { this.dialog = el; }} />
        </div>
      </article>
    );
  }
}
