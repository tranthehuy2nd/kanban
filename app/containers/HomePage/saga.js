import { takeLatest } from 'redux-saga/effects';

export function* welcome() {
  yield (console.log('ADD_NEW_COLUMN hello saga'));
}

export default function* main() {
  yield takeLatest("ADD_NEW_COLUMN", welcome);
}

