/**
 * NotFoundPage
 *
 * This is the page we show when the user visits a url that doesn't have a route
 */

import React from 'react';

export default function NotFound() {
  return (
    <article>
      <h1>Once upon a time. There are some places we know, but we can not go there...</h1>
    </article>
  );
}
