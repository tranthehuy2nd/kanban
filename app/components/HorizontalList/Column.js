import React, { Component } from 'react';
import { Droppable, Draggable } from 'react-beautiful-dnd';
import classnames from 'classnames';
import { Glyphicon } from 'react-bootstrap';
import Task from './Task';
import './style.scss';

const getItemHandlerStyle = (isDragging, draggableStyle) => ({
  ...draggableStyle,
});

const getItemStyle = (isDragging, draggableStyle) => ({
  ...draggableStyle,
});

export default class Column extends Component {
  state = {
    showActionRow: false,
  }

  toogleActionRow = () => {
    this.setState({
      showActionRow: !this.state.showActionRow
    });
  }

  renderColumn = (id) => {
    const tasksList = this.props.datasource('tasks');
    const column = this.props.datasource('columns').get(id);
    const { tasks = [] } = column;
    return (
      <Droppable
        type="TASK"
        droppableId={id}
      >
        {(dropProvided) => (
          <div
            className="board__column-content"
            ref={dropProvided.innerRef}
            {...dropProvided.droppableProps}
          >
            {tasks.map((taskId, index) => (
              <Task key={taskId} item={tasksList.get(taskId)} index={index} />
            ))}
            {dropProvided.placeholder}
          </div>
        )}
      </Droppable>
    );
  }

  render() {
    const { item, index } = this.props;
    return (
      <Draggable key={item.id} draggableId={item.id} index={index}>
        {(provided2, snapshot2) => (
          <div>
            <div
              className={classnames({
                'board__column-header': true,
                'board__column-header--draging': snapshot2.isDragging,
              })}
              ref={provided2.innerRef}
              {...provided2.draggableProps}
              {...provided2.dragHandleProps}
              style={getItemHandlerStyle(
                snapshot2.isDragging,
                provided2.draggableProps.style
              )}
            >
              <div className="text--center block--full-size">{item.name}</div>
              <button onClick={() => { this.toogleActionRow(); }} >
                <Glyphicon glyph="menu-hamburger" />
              </button>
            </div>
            <div
              className={classnames({
                board__column: true,
                'board__column--draging': snapshot2.isDragging,
              })}
              {...provided2.draggableProps}
              style={getItemStyle(
                snapshot2.isDragging,
                provided2.draggableProps.style
              )}
            >
              { this.props.actionRow && this.state.showActionRow && this.props.actionRow(item.id) }
              { this.renderColumn(item.id) }
            </div>
          </div>
        )}
      </Draggable>
    );
  }
}
