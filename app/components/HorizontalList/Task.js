import React, { Component } from 'react';
import { Draggable } from 'react-beautiful-dnd';
import classnames from 'classnames';
import './style.scss';

const getItemHandlerStyle = (isDragging, draggableStyle) => ({
  ...draggableStyle,
});

export default class Task extends Component {
  render() {
    const { item, index } = this.props;
    return (
      <Draggable key={item.id} draggableId={item.id} index={index}>
        {(provided2, snapshot2) => (
          <div
            className={classnames({
              board__task: true,
              'board__task--draging': snapshot2.isDragging,
            })}
            ref={provided2.innerRef}
            {...provided2.draggableProps}
            {...provided2.dragHandleProps}
            style={getItemHandlerStyle(
              snapshot2.isDragging,
              provided2.draggableProps.style
            )}
          >
            {item.name}
          </div>
        )}
      </Draggable>
    );
  }
}
