import React, { Component } from 'react';
// import _ from 'lodash';
import { DragDropContext, Droppable } from 'react-beautiful-dnd';
import Column from './Column';
import './style.scss';

const getListStyle = (isDraggingOver) => ({
  background: isDraggingOver ? null : null,
});

export default class HorizontalList extends Component {
  constructor(props) {
    super(props);
    const columns = this.getColumnsFromProps(props);
    this.state = {
      items: columns,
    };
    this.onDragEnd = this.onDragEnd.bind(this);
    if (this.props.setRefresh) {
      this.props.setRefresh(this.setRefresh());
    }
  }

  onDragEnd(result) {
    if (!result.destination) {
      return;
    }
    if (this.props.onDragEnd) {
      this.props.onDragEnd(result);
    }
    // console.log(result);
    // if (result.type !== 'COLUMN') return;
    // dropped outside the list

    // const items = reorder(
    //   this.state.items,
    //   result.source.index,
    //   result.destination.index
    // );

    // this.setState({
    //   items,
    // });
  }

  setRefresh = () => {
    return () => {
      const columns = this.getColumnsFromProps(this.props);
      this.setState({
        items: columns,
      });
    };
  }

  getColumnsFromProps = (props) => {
    const columnOrder = props.board.columns;
    const columnList = props.datasource('columns');
    const columns = [];
    if (columnList) {
      columnOrder.forEach((index) => {
        columns.push(columnList.get(index));
      });
    }
    return columns;
  }

  // Normally you would want to split things out into separate components.
  // But in this example everything is just done in one place for simplicity
  render() {
    return (
      <DragDropContext onDragEnd={this.onDragEnd}>
        <Droppable droppableId="board" type="COLUMN" direction="horizontal">
          {(provided, snapshot) => (
            <div
              className="board"
              ref={provided.innerRef}
              style={getListStyle(snapshot.isDraggingOver)}
              {...provided.droppableProps}
            >
              {this.state.items.map((item, index) => (
                <Column
                  datasource={this.props.datasource}
                  item={item}
                  key={item.id}
                  tasks={this.props.tasks}
                  index={index}
                  actionRow={this.props.actionRow}
                />
              ))}
              {provided.placeholder}
              <div className="board__action-column">
                {this.props.actionColumn}
              </div>
            </div>
          )}
        </Droppable>
      </DragDropContext>
    );
  }
}
