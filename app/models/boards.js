import { fromJS } from 'immutable';

const initialState = fromJS({
  count: 1,
  list: {
    b0: {
      name: 'board sample',
      columns: [
        'c0', 'c1', 'c2', 'c3'
      ]
    }
  }
});

const actions = {
  SWAP_COLUMNS: 'SWAP_COLUMNS',
  ADD_NEW_COLUMN: 'ADD_NEW_COLUMN',
};

export default function routeReducer(state = initialState, action) {
  switch (action.type) {
    case actions.SWAP_COLUMNS: {
      const columns = state.getIn(['list', action.boardId, 'columns']).toJS();
      const result = Array.from(columns);
      const [removed] = result.splice(action.sourceColumnIndex, 1);
      result.splice(action.destinationColumnIndex, 0, removed);
      return state.setIn(['list', action.boardId, 'columns'], fromJS(result));
    }
    case actions.ADD_NEW_COLUMN: {
      const columns = state.getIn(['list', action.boardId, 'columns']).toJS();
      const result = Array.from(columns);
      result.push(action.columnId);
      return state.setIn(['list', action.boardId, 'columns'], fromJS(result));
    }
    default:
      return state;
  }
}

