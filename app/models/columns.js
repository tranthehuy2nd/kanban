import { fromJS } from 'immutable';

const defaultColumn = {
  id: '',
  name: '',
  tasks: []
};

const initialState = fromJS({
  list: {
    c0: {
      id: 'c0',
      name: '1 Stories',
      tasks: [
        't1', 't2', 't3', 't4'
      ]
    },
    c1: {
      id: 'c1',
      name: '2 Not Started',
      tasks: [
        't5', 't6', 't7', 't8'
      ]
    },
    c2: {
      id: 'c2',
      name: '3 In Progress',
      tasks: [
        't9', 't10', 't11', 't12'
      ]
    },
    c3: {
      id: 'c3',
      name: '4 Done',
      tasks: [
        't13', 't14', 't15', 't16'
      ]
    },
  }
});

const actions = {
  MOVE_TASKS: (state, action) => {
    if (action.sourceColumnId === action.destinationColumnId) {
      const columns = state.getIn(['list', action.sourceColumnId, 'tasks']).toJS();
      const result = Array.from(columns);
      const [removed] = result.splice(action.sourceTaskIndex, 1);
      result.splice(action.destinationTaskIndex, 0, removed);
      return state.setIn(['list', action.sourceColumnId, 'tasks'], fromJS(result));
    }
    const columns = state.getIn(['list', action.sourceColumnId, 'tasks']).toJS();
    const result = Array.from(columns);
    const [removed] = result.splice(action.sourceTaskIndex, 1);

    const columns2 = state.getIn(['list', action.destinationColumnId, 'tasks']).toJS();
    const result2 = Array.from(columns2);
    result2.splice(action.destinationTaskIndex, 0, removed);

    return state.setIn(['list', action.sourceColumnId, 'tasks'], fromJS(result))
      .setIn(['list', action.destinationColumnId, 'tasks'], fromJS(result2));
  },
  ADD_NEW_COLUMN: (state, action) => {
    const newColumn = JSON.parse(JSON.stringify(defaultColumn));
    newColumn.id = action.columnId;
    newColumn.name = action.name;
    return state.setIn(['list', action.columnId], fromJS(newColumn));
  },
  ADD_NEW_TASK: (state, action) => {
    const tasks = state.getIn(['list', action.columnId, 'tasks']).toJS();
    const result = Array.from(tasks);
    result.push(action.taskId);
    return state.setIn(['list', action.columnId, 'tasks'], fromJS(result));
  },
};

export default function routeReducer(state = initialState, action) {
  if (action.type && actions[action.type]) {
    return actions[action.type](state, action);
  }
  return state;
}

