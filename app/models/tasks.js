import { fromJS } from 'immutable';
import { uuid } from '../utils';

const defaultTask = {
  id: '',
  name: '',
};

const initialState = fromJS({
  list: {
    t1: {
      id: uuid(),
      name: 'Stories',
    },
    t2: {
      id: uuid(),
      name: 'Not Started',
    },
    t3: {
      id: uuid(),
      name: 'In Progress',
    },
    t4: {
      id: uuid(),
      name: 'Done',
    },
    t5: {
      id: uuid(),
      name: 'Stories',
    },
    t6: {
      id: uuid(),
      name: 'Not Started',
    },
    t7: {
      id: uuid(),
      name: 'In Progress',
    },
    t8: {
      id: uuid(),
      name: 'Done',
    },
    t9: {
      id: uuid(),
      name: 'Stories',
    },
    t10: {
      id: uuid(),
      name: 'Not Started',
    },
    t11: {
      id: uuid(),
      name: 'In Progress',
    },
    t12: {
      id: uuid(),
      name: 'Done',
    },
    t13: {
      id: uuid(),
      name: 'Stories',
    },
    t14: {
      id: uuid(),
      name: 'Not Started',
    },
    t15: {
      id: uuid(),
      name: 'In Progress',
    },
    t16: {
      id: uuid(),
      name: 'Done',
    },
  }
});

const actions = {
  ADD_NEW_TASK: 'ADD_NEW_TASK',
};

export default function routeReducer(state = initialState, action) {
  switch (action.type) {
    case actions.ADD_NEW_TASK: {
      const newTask = JSON.parse(JSON.stringify(defaultTask));
      newTask.id = action.taskId;
      newTask.name = action.name;
      return state.setIn(['list', action.taskId], fromJS(newTask));
    }
    default:
      return state;
  }
}

